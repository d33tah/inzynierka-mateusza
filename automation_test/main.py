#!/usr/bin/env python3

import unittest

import selenium.webdriver
from selenium.webdriver.common import keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import random

class WebappSeleniumTestCase(unittest.TestCase):
    def setUp(self):

        options = Options()
        options.headless = True
        self.driver = selenium.webdriver.Firefox(options=options)

    def test_chat(self):
        self.driver.get('http://webapp:5000/')
        messagebox = self.driver.find_element_by_name("contents")
        send = self.driver.find_element_by_name("send")        
        randomHash = random.getrandbits(128)

        messagebox.send_keys("",str(randomHash))
        #messagebox.send_keys("aaaa")
        send.click()

        self.assertIn(str(randomHash), self.driver.page_source)
        #self.assertIn("aaaa", self.driver.page_source)
        self.driver.close()

if __name__ == '__main__':
    unittest.main()
