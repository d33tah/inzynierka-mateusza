from django import forms

from webapp.models import ChatMessage

class MessageForm(forms.ModelForm):

	class Meta:
            model = ChatMessage
            fields = ['contents']	
