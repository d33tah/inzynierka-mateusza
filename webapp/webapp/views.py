from django.shortcuts import render
from django.views import View
from django.utils.html import strip_tags

from .models import ChatMessage
from .forms import MessageForm

class MessagesView(View):
    def get(self, request):
        objects = ChatMessage.objects.all()
        return render(request, 'base.html', {"objects": objects})

    def post(self, request, *args, **kwargs):
        new = ChatMessage()
        new.contents =  strip_tags(request.POST.get("contents"))
        new.save()
        return self.get(request)
