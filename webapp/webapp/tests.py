from django.test import TestCase
from django.test import Client

from .models import ChatMessage

class SimpleTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_html_tags_are_stripped_from_message(self):
        response = self.client.post('/',{'contents':'<b>Test message with HTML tags</b>'})
        self.assertEqual(getattr(ChatMessage.objects.first(),"contents"),"Test message with HTML tags")