# Generated by Django 3.1.1 on 2020-09-10 22:39

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('webapp', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chatmessage',
            name='contents',
            field=models.TextField(validators=[django.core.validators.MaxLengthValidator(255)]),
        ),
    ]
