from django.db import models
from django.core.validators import MaxLengthValidator

class ChatMessage(models.Model):
        contents = models.TextField(validators=[MaxLengthValidator(255)])
