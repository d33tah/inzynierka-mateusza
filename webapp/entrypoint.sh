#!/bin/bash

set -euo pipefail  # "bash strict mode"

sleep 5  # zaczekajmy na postgresql - inaczej jest race condition

./manage.py migrate webapp
./manage.py test webapp
exec ./manage.py runserver 0.0.0.0:5000
